// File:	my_pthread_t.h
// Author:	Yujie REN
// Date:	09/23/2017

// name: Kim Russo, Anil Tilve and Grace Perez
// username of iLab: krr89
// iLab Server: grep.cs.rutgers.edu

#ifndef MY_PTHREAD_T_H
#define MY_PTHREAD_T_H

#define _GNU_SOURCE

#define USE_MY_PTHREAD 1

#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>

#define NUM_PRIORITY_LEVELS 5
#define STACK_SIZE 8388608 // 8 MB
#define TIME_QUANTUM 25000 // 25 ms
#define MAINT_INTERVAL 25 // Run maintenance cycle after 25 threads have executed.

#define PRIORITY_HIGHEST 0
#define PRIORITY_LOWEST 4
#define MUTEXT_LOCKED 1
#define MUTEXT_UNLOCKED 0

typedef uint my_pthread_t;

typedef enum state {
	STATE_READY,
	STATE_YIELD,
	STATE_RUNNING,
	STATE_BLOCKED,
	STATE_EXIT,
	STATE_JOIN_WAIT
} state;

typedef struct my_timer_t {
	int time_runs;
	long int start_tt;
	long int first_exe_tt;
	long int last_exe_tt;
	long int end_tt;
} my_timer_t;

typedef struct threadControlBlock {
	int id;
	state thread_state;
	int priority;
	void * ret_val;
	ucontext_t * context;
	my_timer_t timerID;
	struct threadControlBlock * next;
	struct threadControlBlock * prev;
	struct threadControlBlock * joinWait;
} tcb;

typedef struct my_pthread_mutex_t {
	int flag;
} my_pthread_mutex_t;

typedef struct mutexWaitList {
	tcb * threadcb;
	struct mutexWaitList * next;
	int valid;
} mutexWaitList;

typedef struct mutexBlock {
	my_pthread_mutex_t * mutex;
	state mutex_state;
	int owner_orig_prio;
	tcb * owner_tcb;
	struct mutexBlock * next;
	struct mutexWaitList * waitList;
	struct mutexWaitList * waitList_tail;
} mutexBlock;

typedef struct my_pthread_list {
	mutexBlock * head;
	mutexBlock * tail;
	int num_mutex;
} my_pthread_list;

typedef struct sched_queue {
	tcb * head;
	tcb * tail;
	int num_threads;
} sched_queue;

int sched_currThreadID;
sched_queue sched_mlpq[NUM_PRIORITY_LEVELS];
tcb * currentThread;
tcb * mainThread;

int init_mainTCB();
void init_scheduler();
ucontext_t * prepareContext (void (*func)(void), ucontext_t * linkContext, int argc, tcb * threadcb, void * function, void * arg);
void exit_my_pthread();
void run_my_pthread(tcb * threadcb, void *(*function)(void*), void * arg);
void init_my_pthread();
void init_interrupt();
void signal_handler(int sig);
int my_pthread_create(my_pthread_t *thread, pthread_attr_t *attr, void *(*function)(void *), void *arg);
int my_pthread_yield();
void my_pthread_exit(void *value_ptr);
int my_pthread_join(my_pthread_t thread, void **value_ptr);
int addMutexToList(mutexBlock * mblock);
int removeMutexFromList(mutexBlock * mblock);
mutexBlock * findMutex(my_pthread_mutex_t * mutex);
mutexWaitList * findWaitListTCB(mutexBlock * mblock, tcb * threadcb);
int addWaitList(mutexBlock * mblock, tcb * threadcb);
int removeWaitList(mutexBlock * mblock, tcb * threadcb);
void makeReadyWaitList(mutexBlock * mblock);
int my_pthread_mutex_init(my_pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr);
int my_pthread_mutex_lock(my_pthread_mutex_t *mutex);
int my_pthread_mutex_unlock(my_pthread_mutex_t *mutex);
int my_pthread_mutex_destroy(my_pthread_mutex_t *mutex);
tcb * findPriorityInsert(int priority);
void insertAfter(tcb * tcb_old, tcb * tcb_new);
int addThreadToQueue(tcb * threadcb);
int deleteThreadFromQueue(tcb * threadcb);
tcb * reorderQueue(tcb * threadcb);
tcb * findThreadByID(int id);
tcb * findThreadByState(state thrstate);
tcb * getRunningThread();
tcb * getPriorityReadyThread();
void scheduler();

#ifdef USE_MY_PTHREAD
#define pthread_t my_pthread_t
#define pthread_mutex_t my_pthread_mutex_t
#define pthread_create my_pthread_create
#define pthread_exit my_pthread_exit
#define pthread_join my_pthread_join
#define pthread_mutex_init my_pthread_mutex_init
#define pthread_mutex_lock my_pthread_mutex_lock
#define pthread_mutex_unlock my_pthread_mutex_unlock
#define pthread_mutex_destroy my_pthread_mutex_destroy
#endif

#endif
