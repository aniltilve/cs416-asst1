// File:	my_pthread.c
// Author:	Yujie REN
// Date:	09/23/2017

// name: Kim Russo, Anil Tilve and Grace Perez
// username of iLab: krr89
// iLab Server: grep.cs.rutgers.edu

#include "my_pthread_t.h"

int my_pthread_init_done = 0;
ucontext_t my_pthread_mainContext;
tcb * my_pthread_mainTCB;
my_pthread_list my_pthread_mutexList;
static struct itimerval time_slice;

// Initialize the main TCB
int init_mainTCB() {
	// Memory allocation for tcb and its context.
	my_pthread_mainTCB = (tcb*)malloc(sizeof(tcb));
	if (my_pthread_mainTCB == NULL) {
		printf("Failed to allocate memory for main TCB initialization.\n");
		return 1;
	}

	my_pthread_mainTCB->context = (ucontext_t*)malloc(sizeof(ucontext_t));
	my_pthread_mainTCB->thread_state = STATE_READY;
	my_pthread_mainTCB->id = 0;
	my_pthread_mainTCB->priority = PRIORITY_HIGHEST;
	mainThread = my_pthread_mainTCB;

	addThreadToQueue(my_pthread_mainTCB);
	return 1;
}

void init_scheduler() {
	getcontext(&my_pthread_mainContext);
	my_pthread_mainTCB->context = &my_pthread_mainContext;
	currentThread = NULL;
}

// Prepares the context for the context switching.
ucontext_t * prepareContext (void (*func)(void), ucontext_t * linkContext,
	int argc, tcb * threadcb, void * function, void * arg) {

	ucontext_t * context = (ucontext_t*)malloc(sizeof(ucontext_t));
	getcontext(context);

	context->uc_stack.ss_sp = malloc(STACK_SIZE);
	context->uc_stack.ss_size = STACK_SIZE;
	context->uc_link = linkContext;

	if (argc == 0)
		makecontext(context, func, 0);
	else
		makecontext(context, func, 3, threadcb, function, arg);

	return context;
}

void exit_my_pthread() {
	my_pthread_exit(NULL);
}

void run_my_pthread(tcb * threadcb, void *(*function)(void*), void * arg) {
	threadcb->thread_state = STATE_RUNNING;
	currentThread = threadcb;
	threadcb = function(arg);
	threadcb->thread_state = STATE_EXIT;
}

void init_my_pthread() {
	init_mainTCB();
	init_interrupt();
	init_scheduler();
}

void init_interrupt() {
	signal(SIGALRM, signal_handler);
	time_slice.it_value.tv_sec = 0;
	time_slice.it_value.tv_usec = TIME_QUANTUM;
	time_slice.it_interval.tv_sec = 0;
	time_slice.it_interval.tv_usec = TIME_QUANTUM;
	setitimer(ITIMER_REAL, &time_slice, NULL); // Starts the timer.
}

void signal_handler(int sig) {
	scheduler();
}

/*
 * Pthread Note: Your internal implementation of pthreads should have a running
 * and waiting queue. Pthreads that are waiting for a mutex should be moved to
 * the waiting queue. Threads that can be scheduled to run should be in the
 * running queue.
 */

// Creates a pthread that executes function. Attributes are ignored, arg is not.
int my_pthread_create(my_pthread_t *thread, pthread_attr_t *attr, void *(*function)(void *), void *arg) {
	if (my_pthread_init_done == 0) {
		my_pthread_init_done = 1;
		init_my_pthread();
	}

	// If this current thread finishes, the link context will be the thread resumed.
	ucontext_t * linkContext = prepareContext(exit_my_pthread, NULL, 0, NULL, NULL, NULL);
	if (linkContext == NULL) {
		printf("Failed to allocate memory for thread context.\n");
		return 1;
	}

	tcb * threadcb = (tcb*)malloc(sizeof(tcb));
	if (threadcb == NULL) {
		printf("Failed to allocate memory for TCB.\n");
		return 1;
	}

	ucontext_t * threadContext = prepareContext((void(*)(void))run_my_pthread, linkContext, 3, threadcb, function, arg);
	if (threadContext == NULL) {
		printf("Failed to allocate memory for thread context.\n");
		free(threadcb);
		return 1;
	}

	threadcb->context = threadContext;
	threadcb->id = sched_currThreadID++;
	threadcb->priority = PRIORITY_HIGHEST;
	*thread = threadcb->id;
	addThreadToQueue(threadcb); // Adds thread to scheduler queue after creation.

	scheduler();
	return 0;
}

// Explicit call to the my_pthread_t scheduler requesting that the current
// context can be swapped out and another can be scheduled if one is waiting.
int my_pthread_yield() {
	tcb * tcb_curr = getRunningThread();
	tcb * tcb_toRun = getPriorityReadyThread();

	if (tcb_toRun == NULL)
		return 0;

	if (tcb_curr == tcb_toRun)
		return 0;

	tcb_curr->thread_state = STATE_YIELD;
	scheduler();
	return 0;
}

// Explicit call to the my_pthread_t library to end the pthread that called it.
// If the value_ptr isn't NULL, any return value from the thread will be saved.
void my_pthread_exit(void *value_ptr) {
	tcb * threadcb_curr = getRunningThread();
	threadcb_curr->thread_state = STATE_EXIT;

	if (value_ptr != NULL)
		threadcb_curr->ret_val = value_ptr;

	swapcontext(currentThread->context, my_pthread_mainTCB->context);
}

// Call to the my_pthread_t library ensuring that the calling thread will not
// continue execution until the one it references exits. If value_ptr is not
// null, the return value of the exiting thread will be passed back.
int my_pthread_join(my_pthread_t thread, void **value_ptr) {
	tcb * threadcb = findThreadByID(thread);

	if (threadcb != NULL) {
		currentThread->joinWait = threadcb;
		if (threadcb->thread_state != STATE_EXIT) {
			currentThread->thread_state = STATE_JOIN_WAIT;
			scheduler();
		}
		*value_ptr = (void*)threadcb->ret_val;
	} else
		return 1;

	return 0;
}

/*
 * Mutex note: Both the unlock and lock functions should be very fast.
 * If there are any threads that are meant to compete for these functions,
 * my_pthread_yield should be called immediately after running the function in
 * question. Relying on the internal timing will make the function run slower
 * than using yield.
 */

 int addMutexToList(mutexBlock * mblock) {
 	if (my_pthread_mutexList.head == NULL) {
 		mblock->next = NULL;
 		my_pthread_mutexList.head = mblock;
 		my_pthread_mutexList.tail = mblock;
 		my_pthread_mutexList.num_mutex = 1;
 	} else {
 		mblock->next = my_pthread_mutexList.tail->next;
 		my_pthread_mutexList.tail->next = mblock;
 		my_pthread_mutexList.num_mutex++;
 	}

 	return 0;
}

int removeMutexFromList(mutexBlock * mblock) {
	mutexBlock * tmp = my_pthread_mutexList.head;
	mutexBlock * prev = NULL;

	if (tmp->next == NULL) {
		if (tmp == mblock) {
			my_pthread_mutexList.head = NULL;
			my_pthread_mutexList.tail = NULL;
			return 0;
		}
	}

	while (tmp->next != NULL) {
		if (tmp->next == mblock) {
			tmp->next = mblock->next;
			my_pthread_mutexList.num_mutex--;
			break;
		}
		tmp = tmp->next;
	}

	return 0;
}

 mutexBlock * findMutex(my_pthread_mutex_t * mutex) {
 	mutexBlock * mblock = my_pthread_mutexList.head;
 	while (mblock != NULL) {
 		if (mblock->mutex == mutex)
 			break;
 		mblock = mblock->next;
 	}

 	return mblock;
 }

 mutexWaitList * findWaitListTCB(mutexBlock * mblock, tcb * threadcb) {
 	mutexWaitList * waitList = mblock->waitList;
 	while (waitList != NULL) {
 		if (waitList->threadcb == threadcb && waitList->valid == 1)
 			break;
 		waitList = waitList->next;
 	}

 	return waitList;
 }

 int addWaitList(mutexBlock * mblock, tcb * threadcb) {
 	if (mblock->waitList == NULL) {
 		mblock->waitList = (mutexWaitList*)malloc(sizeof(mutexWaitList));
 		mblock->waitList->next = NULL;
 		mblock->waitList_tail = NULL;
 		mblock->waitList->threadcb = threadcb;
 		mblock->waitList->valid = 1;
 	} else {
 		mblock->waitList->valid = 1;
 		mblock->waitList->next = mblock->waitList_tail->next;
 		mblock->waitList_tail->next = mblock->waitList;
 	}

 	return 0;
 }

 int removeWaitList(mutexBlock * mblock, tcb * threadcb) {
 	mutexWaitList * obs = findWaitListTCB(mblock, threadcb);

 	if (obs != NULL)
 		obs->valid = 0;

 	return 0;
 }

 void makeReadyWaitList(mutexBlock * mblock) {
 	mutexWaitList * waitList = mblock->waitList;
 	while (waitList!= NULL) {
 		if (waitList->valid == 1)
 			waitList->threadcb->thread_state = STATE_READY;
 		waitList = waitList->next;
 	}
 }

// Initializes a my_pthread_mutex_t created by the calling thread.
// Attributes are ignored.
int my_pthread_mutex_init(my_pthread_mutex_t *mutex, const pthread_mutexattr_t *mutexattr) {
	mutexBlock * mblock = (mutexBlock*)malloc(sizeof(mutexBlock));
	if (mblock == NULL) {
		printf("Error allocating memory for mutex block.\n");
		return 1;
	}

	mblock->mutex = mutex;
	mblock->mutex_state = MUTEXT_UNLOCKED;
	mblock->owner_tcb = NULL;

	addMutexToList(mblock);
	return 0;
}

// Locks a given mutex, other threads attempting to access this mutex will
// not run until it is unlocked.
int my_pthread_mutex_lock(my_pthread_mutex_t *mutex) {
	mutexBlock * mblock = findMutex(mutex);
	tcb * threadcb_curr = getRunningThread();
	if (mblock != NULL) {
		if (mblock->mutex_state == MUTEXT_UNLOCKED) {
			mblock->owner_tcb = threadcb_curr;
			mblock->mutex_state = MUTEXT_LOCKED;
		} else {
			addWaitList(mblock, threadcb_curr);
			threadcb_curr->thread_state = STATE_BLOCKED;
			my_pthread_yield();
		}
	}

	return 0;
}

// Unlocks a given mutex.
int my_pthread_mutex_unlock(my_pthread_mutex_t *mutex) {
	mutexBlock * mblock = findMutex(mutex);
	tcb * threadcb_curr = getRunningThread();
	if (mblock != NULL) {
		mblock->mutex_state = MUTEXT_UNLOCKED;
		if(mblock->waitList != NULL) {
			makeReadyWaitList(mblock);
			my_pthread_yield();
		}
	}

	return 0;
}

// Destroys a given mutex. Mutex should be unlocked before doing so.
int my_pthread_mutex_destroy(my_pthread_mutex_t *mutex) {
	mutexBlock * mblock = findMutex(mutex);
	if (mutex == NULL)
		return EINVAL;
	else if (mblock->mutex_state != MUTEXT_UNLOCKED)
		return EBUSY;
	else {
		removeMutexFromList(mblock);
		free(mblock);
	}

	return 0;
};

// Scheduler functions.

void insertAfter(tcb * tcb_old, tcb * tcb_new) {
	tcb_new->prev = tcb_old;
	tcb_new->next = tcb_old->next;

	if (tcb_old->next == NULL)
		sched_mlpq[tcb_new->priority].tail = tcb_new;
	else
		tcb_old->next->prev = tcb_new;

	tcb_old->next = tcb_new;
}

int addThreadToQueue(tcb * threadcb) {
	if (sched_mlpq[threadcb->priority].head == NULL) {
		threadcb->next = NULL;
		threadcb->prev = NULL;
		sched_mlpq[threadcb->priority].head = threadcb;
		sched_mlpq[threadcb->priority].tail = threadcb;
		sched_mlpq[threadcb->priority].num_threads = 1;
	} else {
		insertAfter(sched_mlpq[threadcb->priority].tail, threadcb);
		sched_mlpq[threadcb->priority].num_threads++;
	}

	return 0;
}

int deleteThreadFromQueue(tcb * threadcb) {
	if (sched_mlpq[threadcb->priority].head == NULL)
		return 1;

	if (threadcb->prev == NULL)
		sched_mlpq[threadcb->priority].head = threadcb->next;
	else
		threadcb->prev->next = threadcb->next;

	if (threadcb->prev != NULL)
		threadcb->next->prev = threadcb->prev;

	sched_mlpq[threadcb->priority].num_threads--;
	free(threadcb);
	return 0;
}

tcb * findThreadByID(int id) {
	int i;
	tcb * threadcb;
	for (i = 1; i < NUM_PRIORITY_LEVELS+1; i++) {
		threadcb = sched_mlpq[i].head;
		while (threadcb != NULL) {
			if (threadcb->id == id)
				break;
			threadcb = threadcb->next;
		}
	}

	return threadcb;
}

tcb * findThreadByState(state thrstate) {
	int i;
	tcb * threadcb;
	for (i = 0; i < NUM_PRIORITY_LEVELS; i++) {
		threadcb = sched_mlpq[i].head;
		while (threadcb != NULL) {
			if ((threadcb->thread_state == STATE_JOIN_WAIT) && (threadcb->joinWait->thread_state == STATE_EXIT))
				threadcb->thread_state = STATE_READY;
			if (threadcb->thread_state == thrstate)
				break;
			threadcb = threadcb->next;
		}
	}

	if (threadcb != NULL)
		deleteThreadFromQueue(threadcb);

	return threadcb;
}

tcb * getRunningThread() {
	return currentThread;
}

tcb * getPriorityReadyThread() {
	return findThreadByState(STATE_READY);
}

tcb * reorderQueue(tcb * threadcb) {
	tcb * tcb_new = (tcb*)malloc(sizeof(tcb));
	*tcb_new = *threadcb;

	addThreadToQueue(tcb_new);
	deleteThreadFromQueue(threadcb);

	return tcb_new;
}

void scheduler() {
	tcb * threadcb = getRunningThread();

	if (threadcb != NULL) {
		if (threadcb->thread_state == STATE_YIELD)
			addThreadToQueue(threadcb);
		else {
			if (threadcb->thread_state == STATE_RUNNING) {
				int new_priority;
				if (threadcb->priority+1 > PRIORITY_LOWEST)
					new_priority = PRIORITY_LOWEST;
				else
					new_priority = threadcb->priority+1;
				addThreadToQueue(threadcb);
			}
		}
	}

	// Choose another thread to run.
	currentThread = getPriorityReadyThread();

	if (currentThread == NULL)
		run_my_pthread(my_pthread_mainTCB, NULL, NULL);
	currentThread->thread_state = STATE_RUNNING;

	// Swap contexts.
	if (threadcb != NULL)
		swapcontext(threadcb->context, currentThread->context);
	else
		swapcontext(&my_pthread_mainContext, currentThread->context);
}
